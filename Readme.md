# BOOTSTRAP


## Grilla
    container           ->  condenedor, para limitar el ancho de nuestra fila
    container-fuild     ->  contenedor, ocupa todo el ancho

    Dentro de un container siempre debe haber filas y columnas(max 12 columnas)
    
### Lema BootStrap
    Al crear un diseño en Bootstrap hay que tener presente, su lema de `mobile-first sites`, diseña desde móvil hacia dispositivos mas grandes
#### fila
    row                 ->  cada fila, tiene max 12 
    
#### columnas
    col-4               ->  x es el espacio a utlizar respecto a 12
    col-lg-3            ->  ocupe para pantanllas grandes 3 columnas
    class="col col-md-8  offset-md-2" -> col-md-8 -> ocupa 8 columnas offset-md-2 -> ocupa 8 colum del primero a partir de 2 columnas
    class="col col-md-6 offset-md-6"  -> md (dispositivos medianos), es de decir a partir de md que ocupe 6 colum. offset-md6 -> derecha
    

#### pading
    pb-4                -> pading button

#### margin
    mb                  -> margin
    ml-auto             -> calcular el margin hacia la derecha
    mr-auto             -> calcular el margin hacia la izquierda

### Display
    class="d-none d-md-block   -> d-none -> ocultar para dispositivos pequeno. d-md-block -> muestra para dispositivos medianos


## Navbar
    es la cabecera, links para acceder a direntes paginas

    sticky-top --> Es una clase que nos permite mantenerlo fijo
    
## Carousel
    Para manejar imagenes

## Button
    hever -> efecto cuando pasa el cursor.
    .btn-tech:hover {}

### class overlay
    esta clase de posicion sobre el carousel


## Card
    Tarjeta para incluir una imagen y contenido


## Badges (badge)
    Pastillas de texto

## FORM
    form-group --> para los espaciados cuando la pantalla es pequena

    class="form-control form-control-lg"  --> Formulario de tipo INPUT, y de tamano grande


##  ToolTip
    Abbrevarions --> muestra con cierto elemento html o text
    <abbr title="attribute">attr</abbr>   -->   se aplicara para un palabra tecnica.

##  Tooltips
    Mensajes inmediatos sobre cualquier elementos.

##  Scrollspy
    Conociendo la ubicación del usuario en el header
    Uno de los usos es para el header para saber donde esta ubicado.

    Parametros

    `<body data-spy="scroll" data-target="#navbar">`    --> Al inicio, es decir en el body. Data-target -> El id de padre de los items a sincronizar. Va con numeral


    `<div class="collapse navbar-collapse" id="navbar" data-offset="106">` --> Id de Padre de los elementos, data-offset="106" -> posicion inicial de la altura de header.

    `<a class="nav-link" href="#main">Conferencia</a>`   --> href="main" => main: id de de la seccion.

## Modal
    Es una ventana Emergente que se muestra por delante del formulario.
    El codigo de coloca al final de todo nuestro codigo HTML. Buenas practicas.

    `id Modal` --> en la nos permitira realicionar con el elemento HTML (ejemplo boton)
    `<a class="nav-link" data-toggle="modal" data-target="#ModalCompra">Comprar ticket</a>` --> data-toggle-> modal data-target-> Id modal

## Input Group

## Alert 








